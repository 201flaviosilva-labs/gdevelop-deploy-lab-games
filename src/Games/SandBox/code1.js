gdjs.SpritesCode = {};
gdjs.SpritesCode.GDBackGroundObjects1= [];
gdjs.SpritesCode.GDBackGroundObjects2= [];
gdjs.SpritesCode.GDCaoObjects1= [];
gdjs.SpritesCode.GDCaoObjects2= [];
gdjs.SpritesCode.GDTileObjects1= [];
gdjs.SpritesCode.GDTileObjects2= [];
gdjs.SpritesCode.GDCatObjects1= [];
gdjs.SpritesCode.GDCatObjects2= [];
gdjs.SpritesCode.GDLabelObjects1= [];
gdjs.SpritesCode.GDLabelObjects2= [];
gdjs.SpritesCode.GDBackBgButtonObjects1= [];
gdjs.SpritesCode.GDBackBgButtonObjects2= [];
gdjs.SpritesCode.GDBackTxtObjects1= [];
gdjs.SpritesCode.GDBackTxtObjects2= [];

gdjs.SpritesCode.conditionTrue_0 = {val:false};
gdjs.SpritesCode.condition0IsTrue_0 = {val:false};
gdjs.SpritesCode.condition1IsTrue_0 = {val:false};


gdjs.SpritesCode.mapOfGDgdjs_46SpritesCode_46GDBackBgButtonObjects1Objects = Hashtable.newFrom({"BackBgButton": gdjs.SpritesCode.GDBackBgButtonObjects1});gdjs.SpritesCode.eventsList0 = function(runtimeScene) {

{


gdjs.SpritesCode.condition0IsTrue_0.val = false;
{
gdjs.SpritesCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num0");
}if (gdjs.SpritesCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Cao"), gdjs.SpritesCode.GDCaoObjects1);
gdjs.copyArray(runtimeScene.getObjects("Cat"), gdjs.SpritesCode.GDCatObjects1);
{for(var i = 0, len = gdjs.SpritesCode.GDCaoObjects1.length ;i < len;++i) {
    gdjs.SpritesCode.GDCaoObjects1[i].setAnimation(0);
}
}{for(var i = 0, len = gdjs.SpritesCode.GDCatObjects1.length ;i < len;++i) {
    gdjs.SpritesCode.GDCatObjects1[i].setAnimation(0);
}
}}

}


{


gdjs.SpritesCode.condition0IsTrue_0.val = false;
{
gdjs.SpritesCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num1");
}if (gdjs.SpritesCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Cao"), gdjs.SpritesCode.GDCaoObjects1);
gdjs.copyArray(runtimeScene.getObjects("Cat"), gdjs.SpritesCode.GDCatObjects1);
{for(var i = 0, len = gdjs.SpritesCode.GDCaoObjects1.length ;i < len;++i) {
    gdjs.SpritesCode.GDCaoObjects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.SpritesCode.GDCatObjects1.length ;i < len;++i) {
    gdjs.SpritesCode.GDCatObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.SpritesCode.condition0IsTrue_0.val = false;
{
gdjs.SpritesCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num2");
}if (gdjs.SpritesCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Cao"), gdjs.SpritesCode.GDCaoObjects1);
gdjs.copyArray(runtimeScene.getObjects("Cat"), gdjs.SpritesCode.GDCatObjects1);
{for(var i = 0, len = gdjs.SpritesCode.GDCaoObjects1.length ;i < len;++i) {
    gdjs.SpritesCode.GDCaoObjects1[i].setAnimation(2);
}
}{for(var i = 0, len = gdjs.SpritesCode.GDCatObjects1.length ;i < len;++i) {
    gdjs.SpritesCode.GDCatObjects1[i].setAnimation(2);
}
}}

}


{


gdjs.SpritesCode.condition0IsTrue_0.val = false;
{
gdjs.SpritesCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num3");
}if (gdjs.SpritesCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Cao"), gdjs.SpritesCode.GDCaoObjects1);
gdjs.copyArray(runtimeScene.getObjects("Cat"), gdjs.SpritesCode.GDCatObjects1);
{for(var i = 0, len = gdjs.SpritesCode.GDCaoObjects1.length ;i < len;++i) {
    gdjs.SpritesCode.GDCaoObjects1[i].setAnimation(3);
}
}{for(var i = 0, len = gdjs.SpritesCode.GDCatObjects1.length ;i < len;++i) {
    gdjs.SpritesCode.GDCatObjects1[i].setAnimation(3);
}
}}

}


{


gdjs.SpritesCode.condition0IsTrue_0.val = false;
{
gdjs.SpritesCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num4");
}if (gdjs.SpritesCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Cao"), gdjs.SpritesCode.GDCaoObjects1);
gdjs.copyArray(runtimeScene.getObjects("Cat"), gdjs.SpritesCode.GDCatObjects1);
{for(var i = 0, len = gdjs.SpritesCode.GDCaoObjects1.length ;i < len;++i) {
    gdjs.SpritesCode.GDCaoObjects1[i].setAnimation(4);
}
}{for(var i = 0, len = gdjs.SpritesCode.GDCatObjects1.length ;i < len;++i) {
    gdjs.SpritesCode.GDCatObjects1[i].setAnimation(4);
}
}}

}


{


gdjs.SpritesCode.condition0IsTrue_0.val = false;
{
gdjs.SpritesCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num5");
}if (gdjs.SpritesCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Cao"), gdjs.SpritesCode.GDCaoObjects1);
gdjs.copyArray(runtimeScene.getObjects("Cat"), gdjs.SpritesCode.GDCatObjects1);
{for(var i = 0, len = gdjs.SpritesCode.GDCaoObjects1.length ;i < len;++i) {
    gdjs.SpritesCode.GDCaoObjects1[i].setAnimation(5);
}
}{for(var i = 0, len = gdjs.SpritesCode.GDCatObjects1.length ;i < len;++i) {
    gdjs.SpritesCode.GDCatObjects1[i].setAnimation(5);
}
}}

}


{


gdjs.SpritesCode.condition0IsTrue_0.val = false;
{
gdjs.SpritesCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num6");
}if (gdjs.SpritesCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Cao"), gdjs.SpritesCode.GDCaoObjects1);
gdjs.copyArray(runtimeScene.getObjects("Cat"), gdjs.SpritesCode.GDCatObjects1);
{for(var i = 0, len = gdjs.SpritesCode.GDCaoObjects1.length ;i < len;++i) {
    gdjs.SpritesCode.GDCaoObjects1[i].setAnimation(6);
}
}{for(var i = 0, len = gdjs.SpritesCode.GDCatObjects1.length ;i < len;++i) {
    gdjs.SpritesCode.GDCatObjects1[i].setAnimation(6);
}
}}

}


{


gdjs.SpritesCode.condition0IsTrue_0.val = false;
{
gdjs.SpritesCode.condition0IsTrue_0.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Num7");
}if (gdjs.SpritesCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Cao"), gdjs.SpritesCode.GDCaoObjects1);
gdjs.copyArray(runtimeScene.getObjects("Cat"), gdjs.SpritesCode.GDCatObjects1);
{for(var i = 0, len = gdjs.SpritesCode.GDCaoObjects1.length ;i < len;++i) {
    gdjs.SpritesCode.GDCaoObjects1[i].setAnimation(7);
}
}{for(var i = 0, len = gdjs.SpritesCode.GDCatObjects1.length ;i < len;++i) {
    gdjs.SpritesCode.GDCatObjects1[i].setAnimation(7);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("BackBgButton"), gdjs.SpritesCode.GDBackBgButtonObjects1);

gdjs.SpritesCode.condition0IsTrue_0.val = false;
{
gdjs.SpritesCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.SpritesCode.mapOfGDgdjs_46SpritesCode_46GDBackBgButtonObjects1Objects, runtimeScene, true, false);
}if (gdjs.SpritesCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start Menu", false);
}}

}


};

gdjs.SpritesCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.SpritesCode.GDBackGroundObjects1.length = 0;
gdjs.SpritesCode.GDBackGroundObjects2.length = 0;
gdjs.SpritesCode.GDCaoObjects1.length = 0;
gdjs.SpritesCode.GDCaoObjects2.length = 0;
gdjs.SpritesCode.GDTileObjects1.length = 0;
gdjs.SpritesCode.GDTileObjects2.length = 0;
gdjs.SpritesCode.GDCatObjects1.length = 0;
gdjs.SpritesCode.GDCatObjects2.length = 0;
gdjs.SpritesCode.GDLabelObjects1.length = 0;
gdjs.SpritesCode.GDLabelObjects2.length = 0;
gdjs.SpritesCode.GDBackBgButtonObjects1.length = 0;
gdjs.SpritesCode.GDBackBgButtonObjects2.length = 0;
gdjs.SpritesCode.GDBackTxtObjects1.length = 0;
gdjs.SpritesCode.GDBackTxtObjects2.length = 0;

gdjs.SpritesCode.eventsList0(runtimeScene);
return;

}

gdjs['SpritesCode'] = gdjs.SpritesCode;
