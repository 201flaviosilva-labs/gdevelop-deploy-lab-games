gdjs.ParticlesCode = {};
gdjs.ParticlesCode.GDBasicPointObjects1= [];
gdjs.ParticlesCode.GDBasicPointObjects2= [];
gdjs.ParticlesCode.GDBasicLineObjects1= [];
gdjs.ParticlesCode.GDBasicLineObjects2= [];
gdjs.ParticlesCode.GDBasicTextureObjects1= [];
gdjs.ParticlesCode.GDBasicTextureObjects2= [];
gdjs.ParticlesCode.GDMagic2Objects1= [];
gdjs.ParticlesCode.GDMagic2Objects2= [];
gdjs.ParticlesCode.GDStarSparksObjects1= [];
gdjs.ParticlesCode.GDStarSparksObjects2= [];
gdjs.ParticlesCode.GDBackBgButtonObjects1= [];
gdjs.ParticlesCode.GDBackBgButtonObjects2= [];
gdjs.ParticlesCode.GDBackTxtObjects1= [];
gdjs.ParticlesCode.GDBackTxtObjects2= [];

gdjs.ParticlesCode.conditionTrue_0 = {val:false};
gdjs.ParticlesCode.condition0IsTrue_0 = {val:false};
gdjs.ParticlesCode.condition1IsTrue_0 = {val:false};


gdjs.ParticlesCode.mapOfGDgdjs_46ParticlesCode_46GDBackBgButtonObjects1Objects = Hashtable.newFrom({"BackBgButton": gdjs.ParticlesCode.GDBackBgButtonObjects1});gdjs.ParticlesCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("BackBgButton"), gdjs.ParticlesCode.GDBackBgButtonObjects1);

gdjs.ParticlesCode.condition0IsTrue_0.val = false;
{
gdjs.ParticlesCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.ParticlesCode.mapOfGDgdjs_46ParticlesCode_46GDBackBgButtonObjects1Objects, runtimeScene, true, false);
}if (gdjs.ParticlesCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Start Menu", false);
}}

}


};

gdjs.ParticlesCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.ParticlesCode.GDBasicPointObjects1.length = 0;
gdjs.ParticlesCode.GDBasicPointObjects2.length = 0;
gdjs.ParticlesCode.GDBasicLineObjects1.length = 0;
gdjs.ParticlesCode.GDBasicLineObjects2.length = 0;
gdjs.ParticlesCode.GDBasicTextureObjects1.length = 0;
gdjs.ParticlesCode.GDBasicTextureObjects2.length = 0;
gdjs.ParticlesCode.GDMagic2Objects1.length = 0;
gdjs.ParticlesCode.GDMagic2Objects2.length = 0;
gdjs.ParticlesCode.GDStarSparksObjects1.length = 0;
gdjs.ParticlesCode.GDStarSparksObjects2.length = 0;
gdjs.ParticlesCode.GDBackBgButtonObjects1.length = 0;
gdjs.ParticlesCode.GDBackBgButtonObjects2.length = 0;
gdjs.ParticlesCode.GDBackTxtObjects1.length = 0;
gdjs.ParticlesCode.GDBackTxtObjects2.length = 0;

gdjs.ParticlesCode.eventsList0(runtimeScene);
return;

}

gdjs['ParticlesCode'] = gdjs.ParticlesCode;
