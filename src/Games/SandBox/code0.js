gdjs.Start_32MenuCode = {};
gdjs.Start_32MenuCode.GDTitleObjects1= [];
gdjs.Start_32MenuCode.GDTitleObjects2= [];
gdjs.Start_32MenuCode.GDSpritesBgButtonObjects1= [];
gdjs.Start_32MenuCode.GDSpritesBgButtonObjects2= [];
gdjs.Start_32MenuCode.GDSpritesTxtObjects1= [];
gdjs.Start_32MenuCode.GDSpritesTxtObjects2= [];
gdjs.Start_32MenuCode.GDParticleBgButtonObjects1= [];
gdjs.Start_32MenuCode.GDParticleBgButtonObjects2= [];
gdjs.Start_32MenuCode.GDParticlesTxtObjects1= [];
gdjs.Start_32MenuCode.GDParticlesTxtObjects2= [];

gdjs.Start_32MenuCode.conditionTrue_0 = {val:false};
gdjs.Start_32MenuCode.condition0IsTrue_0 = {val:false};
gdjs.Start_32MenuCode.condition1IsTrue_0 = {val:false};


gdjs.Start_32MenuCode.mapOfGDgdjs_46Start_9532MenuCode_46GDSpritesBgButtonObjects1Objects = Hashtable.newFrom({"SpritesBgButton": gdjs.Start_32MenuCode.GDSpritesBgButtonObjects1});gdjs.Start_32MenuCode.mapOfGDgdjs_46Start_9532MenuCode_46GDParticleBgButtonObjects1Objects = Hashtable.newFrom({"ParticleBgButton": gdjs.Start_32MenuCode.GDParticleBgButtonObjects1});gdjs.Start_32MenuCode.eventsList0 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("SpritesBgButton"), gdjs.Start_32MenuCode.GDSpritesBgButtonObjects1);

gdjs.Start_32MenuCode.condition0IsTrue_0.val = false;
{
gdjs.Start_32MenuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Start_32MenuCode.mapOfGDgdjs_46Start_9532MenuCode_46GDSpritesBgButtonObjects1Objects, runtimeScene, true, false);
}if (gdjs.Start_32MenuCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Sprites", false);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("ParticleBgButton"), gdjs.Start_32MenuCode.GDParticleBgButtonObjects1);

gdjs.Start_32MenuCode.condition0IsTrue_0.val = false;
{
gdjs.Start_32MenuCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.Start_32MenuCode.mapOfGDgdjs_46Start_9532MenuCode_46GDParticleBgButtonObjects1Objects, runtimeScene, true, false);
}if (gdjs.Start_32MenuCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Particles", false);
}}

}


};

gdjs.Start_32MenuCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.Start_32MenuCode.GDTitleObjects1.length = 0;
gdjs.Start_32MenuCode.GDTitleObjects2.length = 0;
gdjs.Start_32MenuCode.GDSpritesBgButtonObjects1.length = 0;
gdjs.Start_32MenuCode.GDSpritesBgButtonObjects2.length = 0;
gdjs.Start_32MenuCode.GDSpritesTxtObjects1.length = 0;
gdjs.Start_32MenuCode.GDSpritesTxtObjects2.length = 0;
gdjs.Start_32MenuCode.GDParticleBgButtonObjects1.length = 0;
gdjs.Start_32MenuCode.GDParticleBgButtonObjects2.length = 0;
gdjs.Start_32MenuCode.GDParticlesTxtObjects1.length = 0;
gdjs.Start_32MenuCode.GDParticlesTxtObjects2.length = 0;

gdjs.Start_32MenuCode.eventsList0(runtimeScene);
return;

}

gdjs['Start_32MenuCode'] = gdjs.Start_32MenuCode;
