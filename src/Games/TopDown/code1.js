gdjs.Nivel1Code = {};
gdjs.Nivel1Code.GDPlayerObjects1= [];
gdjs.Nivel1Code.GDPlayerObjects2= [];
gdjs.Nivel1Code.GDPlayerObjects3= [];
gdjs.Nivel1Code.GDBulletObjects1= [];
gdjs.Nivel1Code.GDBulletObjects2= [];
gdjs.Nivel1Code.GDBulletObjects3= [];
gdjs.Nivel1Code.GDEnemyObjects1= [];
gdjs.Nivel1Code.GDEnemyObjects2= [];
gdjs.Nivel1Code.GDEnemyObjects3= [];
gdjs.Nivel1Code.GDEdgeObjects1= [];
gdjs.Nivel1Code.GDEdgeObjects2= [];
gdjs.Nivel1Code.GDEdgeObjects3= [];

gdjs.Nivel1Code.conditionTrue_0 = {val:false};
gdjs.Nivel1Code.condition0IsTrue_0 = {val:false};
gdjs.Nivel1Code.condition1IsTrue_0 = {val:false};
gdjs.Nivel1Code.condition2IsTrue_0 = {val:false};
gdjs.Nivel1Code.condition3IsTrue_0 = {val:false};
gdjs.Nivel1Code.conditionTrue_1 = {val:false};
gdjs.Nivel1Code.condition0IsTrue_1 = {val:false};
gdjs.Nivel1Code.condition1IsTrue_1 = {val:false};
gdjs.Nivel1Code.condition2IsTrue_1 = {val:false};
gdjs.Nivel1Code.condition3IsTrue_1 = {val:false};


gdjs.Nivel1Code.eventsList0 = function(runtimeScene) {

{


gdjs.Nivel1Code.condition0IsTrue_0.val = false;
{
gdjs.Nivel1Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Nivel1Code.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Enemy"), gdjs.Nivel1Code.GDEnemyObjects2);
{for(var i = 0, len = gdjs.Nivel1Code.GDEnemyObjects2.length ;i < len;++i) {
    gdjs.Nivel1Code.GDEnemyObjects2[i].addPolarForce(-(45), gdjs.random(600) + 10, 1);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}}

}


{


gdjs.Nivel1Code.condition0IsTrue_0.val = false;
{
gdjs.Nivel1Code.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.Nivel1Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Nivel2", false);
}}

}


};gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDPlayerObjects1Objects = Hashtable.newFrom({"Player": gdjs.Nivel1Code.GDPlayerObjects1});gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDEdgeObjects1Objects = Hashtable.newFrom({"Edge": gdjs.Nivel1Code.GDEdgeObjects1});gdjs.Nivel1Code.eventsList1 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Nivel1Code.GDPlayerObjects2);

gdjs.Nivel1Code.condition0IsTrue_0.val = false;
gdjs.Nivel1Code.condition1IsTrue_0.val = false;
{
{gdjs.Nivel1Code.conditionTrue_1 = gdjs.Nivel1Code.condition0IsTrue_0;
gdjs.Nivel1Code.condition0IsTrue_1.val = false;
gdjs.Nivel1Code.condition1IsTrue_1.val = false;
{
gdjs.Nivel1Code.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
if( gdjs.Nivel1Code.condition0IsTrue_1.val ) {
    gdjs.Nivel1Code.conditionTrue_1.val = true;
}
}
{
gdjs.Nivel1Code.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "a");
if( gdjs.Nivel1Code.condition1IsTrue_1.val ) {
    gdjs.Nivel1Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.Nivel1Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Nivel1Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Nivel1Code.GDPlayerObjects2[i].getX() > 0 ) {
        gdjs.Nivel1Code.condition1IsTrue_0.val = true;
        gdjs.Nivel1Code.GDPlayerObjects2[k] = gdjs.Nivel1Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Nivel1Code.GDPlayerObjects2.length = k;}}
if (gdjs.Nivel1Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Nivel1Code.GDPlayerObjects2 */
{}{for(var i = 0, len = gdjs.Nivel1Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Nivel1Code.GDPlayerObjects2[i].setX(gdjs.Nivel1Code.GDPlayerObjects2[i].getX() - (5));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Nivel1Code.GDPlayerObjects2);

gdjs.Nivel1Code.condition0IsTrue_0.val = false;
gdjs.Nivel1Code.condition1IsTrue_0.val = false;
{
{gdjs.Nivel1Code.conditionTrue_1 = gdjs.Nivel1Code.condition0IsTrue_0;
gdjs.Nivel1Code.condition0IsTrue_1.val = false;
gdjs.Nivel1Code.condition1IsTrue_1.val = false;
{
gdjs.Nivel1Code.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
if( gdjs.Nivel1Code.condition0IsTrue_1.val ) {
    gdjs.Nivel1Code.conditionTrue_1.val = true;
}
}
{
gdjs.Nivel1Code.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "d");
if( gdjs.Nivel1Code.condition1IsTrue_1.val ) {
    gdjs.Nivel1Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.Nivel1Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Nivel1Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Nivel1Code.GDPlayerObjects2[i].getX() < 800 - (gdjs.Nivel1Code.GDPlayerObjects2[i].getWidth()) ) {
        gdjs.Nivel1Code.condition1IsTrue_0.val = true;
        gdjs.Nivel1Code.GDPlayerObjects2[k] = gdjs.Nivel1Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Nivel1Code.GDPlayerObjects2.length = k;}}
if (gdjs.Nivel1Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Nivel1Code.GDPlayerObjects2 */
{}{for(var i = 0, len = gdjs.Nivel1Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Nivel1Code.GDPlayerObjects2[i].setX(gdjs.Nivel1Code.GDPlayerObjects2[i].getX() + (5));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Nivel1Code.GDPlayerObjects2);

gdjs.Nivel1Code.condition0IsTrue_0.val = false;
gdjs.Nivel1Code.condition1IsTrue_0.val = false;
{
{gdjs.Nivel1Code.conditionTrue_1 = gdjs.Nivel1Code.condition0IsTrue_0;
gdjs.Nivel1Code.condition0IsTrue_1.val = false;
gdjs.Nivel1Code.condition1IsTrue_1.val = false;
{
gdjs.Nivel1Code.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if( gdjs.Nivel1Code.condition0IsTrue_1.val ) {
    gdjs.Nivel1Code.conditionTrue_1.val = true;
}
}
{
gdjs.Nivel1Code.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "w");
if( gdjs.Nivel1Code.condition1IsTrue_1.val ) {
    gdjs.Nivel1Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.Nivel1Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Nivel1Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Nivel1Code.GDPlayerObjects2[i].getY() > 0 ) {
        gdjs.Nivel1Code.condition1IsTrue_0.val = true;
        gdjs.Nivel1Code.GDPlayerObjects2[k] = gdjs.Nivel1Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Nivel1Code.GDPlayerObjects2.length = k;}}
if (gdjs.Nivel1Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Nivel1Code.GDPlayerObjects2 */
{}{for(var i = 0, len = gdjs.Nivel1Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Nivel1Code.GDPlayerObjects2[i].setY(gdjs.Nivel1Code.GDPlayerObjects2[i].getY() - (5));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Nivel1Code.GDPlayerObjects2);

gdjs.Nivel1Code.condition0IsTrue_0.val = false;
gdjs.Nivel1Code.condition1IsTrue_0.val = false;
{
{gdjs.Nivel1Code.conditionTrue_1 = gdjs.Nivel1Code.condition0IsTrue_0;
gdjs.Nivel1Code.condition0IsTrue_1.val = false;
gdjs.Nivel1Code.condition1IsTrue_1.val = false;
{
gdjs.Nivel1Code.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Down");
if( gdjs.Nivel1Code.condition0IsTrue_1.val ) {
    gdjs.Nivel1Code.conditionTrue_1.val = true;
}
}
{
gdjs.Nivel1Code.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "s");
if( gdjs.Nivel1Code.condition1IsTrue_1.val ) {
    gdjs.Nivel1Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.Nivel1Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Nivel1Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Nivel1Code.GDPlayerObjects2[i].getY() < 600 - (gdjs.Nivel1Code.GDPlayerObjects2[i].getHeight()) ) {
        gdjs.Nivel1Code.condition1IsTrue_0.val = true;
        gdjs.Nivel1Code.GDPlayerObjects2[k] = gdjs.Nivel1Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Nivel1Code.GDPlayerObjects2.length = k;}}
if (gdjs.Nivel1Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Nivel1Code.GDPlayerObjects2 */
{}{for(var i = 0, len = gdjs.Nivel1Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Nivel1Code.GDPlayerObjects2[i].setY(gdjs.Nivel1Code.GDPlayerObjects2[i].getY() + (5));
}
}}

}


{


{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Nivel1Code.GDPlayerObjects2);
{for(var i = 0, len = gdjs.Nivel1Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Nivel1Code.GDPlayerObjects2[i].rotateTowardPosition(gdjs.evtTools.input.getMouseX(runtimeScene, "", 0), gdjs.evtTools.input.getMouseY(runtimeScene, "", 0), 0, runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Edge"), gdjs.Nivel1Code.GDEdgeObjects1);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Nivel1Code.GDPlayerObjects1);

gdjs.Nivel1Code.condition0IsTrue_0.val = false;
{
gdjs.Nivel1Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDPlayerObjects1Objects, gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDEdgeObjects1Objects, false, runtimeScene, false);
}if (gdjs.Nivel1Code.condition0IsTrue_0.val) {
}

}


};gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Nivel1Code.GDPlayerObjects2});gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDEdgeObjects2Objects = Hashtable.newFrom({"Edge": gdjs.Nivel1Code.GDEdgeObjects2});gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDBulletObjects2Objects = Hashtable.newFrom({"Bullet": gdjs.Nivel1Code.GDBulletObjects2});gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDBulletObjects2Objects = Hashtable.newFrom({"Bullet": gdjs.Nivel1Code.GDBulletObjects2});gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDEnemyObjects2Objects = Hashtable.newFrom({"Enemy": gdjs.Nivel1Code.GDEnemyObjects2});gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDBulletObjects1Objects = Hashtable.newFrom({"Bullet": gdjs.Nivel1Code.GDBulletObjects1});gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDEdgeObjects1Objects = Hashtable.newFrom({"Edge": gdjs.Nivel1Code.GDEdgeObjects1});gdjs.Nivel1Code.eventsList2 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Edge"), gdjs.Nivel1Code.GDEdgeObjects2);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Nivel1Code.GDPlayerObjects2);

gdjs.Nivel1Code.condition0IsTrue_0.val = false;
gdjs.Nivel1Code.condition1IsTrue_0.val = false;
gdjs.Nivel1Code.condition2IsTrue_0.val = false;
{
gdjs.Nivel1Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Nivel1Code.condition0IsTrue_0.val ) {
{
gdjs.Nivel1Code.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDPlayerObjects2Objects, gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDEdgeObjects2Objects, true, runtimeScene, false);
}if ( gdjs.Nivel1Code.condition1IsTrue_0.val ) {
{
{gdjs.Nivel1Code.conditionTrue_1 = gdjs.Nivel1Code.condition2IsTrue_0;
gdjs.Nivel1Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8384724);
}
}}
}
if (gdjs.Nivel1Code.condition2IsTrue_0.val) {
/* Reuse gdjs.Nivel1Code.GDPlayerObjects2 */
gdjs.Nivel1Code.GDBulletObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDBulletObjects2Objects, (( gdjs.Nivel1Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Nivel1Code.GDPlayerObjects2[0].getPointX("")) + ((( gdjs.Nivel1Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Nivel1Code.GDPlayerObjects2[0].getWidth()) / 2) + ((( gdjs.Nivel1Code.GDBulletObjects2.length === 0 ) ? 0 :gdjs.Nivel1Code.GDBulletObjects2[0].getWidth()) / 2), (( gdjs.Nivel1Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Nivel1Code.GDPlayerObjects2[0].getPointY("")) + ((( gdjs.Nivel1Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Nivel1Code.GDPlayerObjects2[0].getHeight()) / 2) + ((( gdjs.Nivel1Code.GDBulletObjects2.length === 0 ) ? 0 :gdjs.Nivel1Code.GDBulletObjects2[0].getHeight()) / 2), "");
}{for(var i = 0, len = gdjs.Nivel1Code.GDBulletObjects2.length ;i < len;++i) {
    gdjs.Nivel1Code.GDBulletObjects2[i].addPolarForce((( gdjs.Nivel1Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Nivel1Code.GDPlayerObjects2[0].getDirectionOrAngle()), 1000, 1);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Bullet"), gdjs.Nivel1Code.GDBulletObjects2);
gdjs.copyArray(runtimeScene.getObjects("Enemy"), gdjs.Nivel1Code.GDEnemyObjects2);

gdjs.Nivel1Code.condition0IsTrue_0.val = false;
{
gdjs.Nivel1Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDBulletObjects2Objects, gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDEnemyObjects2Objects, false, runtimeScene, false);
}if (gdjs.Nivel1Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Nivel1Code.GDBulletObjects2 */
/* Reuse gdjs.Nivel1Code.GDEnemyObjects2 */
{for(var i = 0, len = gdjs.Nivel1Code.GDBulletObjects2.length ;i < len;++i) {
    gdjs.Nivel1Code.GDBulletObjects2[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.Nivel1Code.GDEnemyObjects2.length ;i < len;++i) {
    gdjs.Nivel1Code.GDEnemyObjects2[i].deleteFromScene(runtimeScene);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).sub(1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Bullet"), gdjs.Nivel1Code.GDBulletObjects1);
gdjs.copyArray(runtimeScene.getObjects("Edge"), gdjs.Nivel1Code.GDEdgeObjects1);

gdjs.Nivel1Code.condition0IsTrue_0.val = false;
{
gdjs.Nivel1Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDBulletObjects1Objects, gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDEdgeObjects1Objects, false, runtimeScene, false);
}if (gdjs.Nivel1Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Nivel1Code.GDBulletObjects1 */
{for(var i = 0, len = gdjs.Nivel1Code.GDBulletObjects1.length ;i < len;++i) {
    gdjs.Nivel1Code.GDBulletObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDEnemyObjects2Objects = Hashtable.newFrom({"Enemy": gdjs.Nivel1Code.GDEnemyObjects2});gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDEdgeObjects2Objects = Hashtable.newFrom({"Edge": gdjs.Nivel1Code.GDEdgeObjects2});gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDEdgeObjects2Objects = Hashtable.newFrom({"Edge": gdjs.Nivel1Code.GDEdgeObjects2});gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDEnemyObjects1Objects = Hashtable.newFrom({"Enemy": gdjs.Nivel1Code.GDEnemyObjects1});gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDPlayerObjects1Objects = Hashtable.newFrom({"Player": gdjs.Nivel1Code.GDPlayerObjects1});gdjs.Nivel1Code.eventsList3 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Edge"), gdjs.Nivel1Code.GDEdgeObjects2);
gdjs.copyArray(runtimeScene.getObjects("Enemy"), gdjs.Nivel1Code.GDEnemyObjects2);

gdjs.Nivel1Code.condition0IsTrue_0.val = false;
{
gdjs.Nivel1Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDEnemyObjects2Objects, gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDEdgeObjects2Objects, false, runtimeScene, false);
}if (gdjs.Nivel1Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Nivel1Code.GDEdgeObjects2 */
/* Reuse gdjs.Nivel1Code.GDEnemyObjects2 */
{for(var i = 0, len = gdjs.Nivel1Code.GDEnemyObjects2.length ;i < len;++i) {
    gdjs.Nivel1Code.GDEnemyObjects2[i].getBehavior("Bounce").BounceOff(gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDEdgeObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Enemy"), gdjs.Nivel1Code.GDEnemyObjects1);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Nivel1Code.GDPlayerObjects1);

gdjs.Nivel1Code.condition0IsTrue_0.val = false;
{
gdjs.Nivel1Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDEnemyObjects1Objects, gdjs.Nivel1Code.mapOfGDgdjs_46Nivel1Code_46GDPlayerObjects1Objects, false, runtimeScene, false);
}if (gdjs.Nivel1Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Menu", false);
}}

}


};gdjs.Nivel1Code.eventsList4 = function(runtimeScene) {

{


gdjs.Nivel1Code.eventsList0(runtimeScene);
}


{


gdjs.Nivel1Code.eventsList1(runtimeScene);
}


{


gdjs.Nivel1Code.eventsList2(runtimeScene);
}


{


gdjs.Nivel1Code.eventsList3(runtimeScene);
}


};

gdjs.Nivel1Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.Nivel1Code.GDPlayerObjects1.length = 0;
gdjs.Nivel1Code.GDPlayerObjects2.length = 0;
gdjs.Nivel1Code.GDPlayerObjects3.length = 0;
gdjs.Nivel1Code.GDBulletObjects1.length = 0;
gdjs.Nivel1Code.GDBulletObjects2.length = 0;
gdjs.Nivel1Code.GDBulletObjects3.length = 0;
gdjs.Nivel1Code.GDEnemyObjects1.length = 0;
gdjs.Nivel1Code.GDEnemyObjects2.length = 0;
gdjs.Nivel1Code.GDEnemyObjects3.length = 0;
gdjs.Nivel1Code.GDEdgeObjects1.length = 0;
gdjs.Nivel1Code.GDEdgeObjects2.length = 0;
gdjs.Nivel1Code.GDEdgeObjects3.length = 0;

gdjs.Nivel1Code.eventsList4(runtimeScene);
return;

}

gdjs['Nivel1Code'] = gdjs.Nivel1Code;
