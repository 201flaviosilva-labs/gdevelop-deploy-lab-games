gdjs.Nivel3Code = {};
gdjs.Nivel3Code.GDPlayerObjects1= [];
gdjs.Nivel3Code.GDPlayerObjects2= [];
gdjs.Nivel3Code.GDPlayerObjects3= [];
gdjs.Nivel3Code.GDBulletObjects1= [];
gdjs.Nivel3Code.GDBulletObjects2= [];
gdjs.Nivel3Code.GDBulletObjects3= [];
gdjs.Nivel3Code.GDEnemyObjects1= [];
gdjs.Nivel3Code.GDEnemyObjects2= [];
gdjs.Nivel3Code.GDEnemyObjects3= [];
gdjs.Nivel3Code.GDEdgeObjects1= [];
gdjs.Nivel3Code.GDEdgeObjects2= [];
gdjs.Nivel3Code.GDEdgeObjects3= [];

gdjs.Nivel3Code.conditionTrue_0 = {val:false};
gdjs.Nivel3Code.condition0IsTrue_0 = {val:false};
gdjs.Nivel3Code.condition1IsTrue_0 = {val:false};
gdjs.Nivel3Code.condition2IsTrue_0 = {val:false};
gdjs.Nivel3Code.condition3IsTrue_0 = {val:false};
gdjs.Nivel3Code.conditionTrue_1 = {val:false};
gdjs.Nivel3Code.condition0IsTrue_1 = {val:false};
gdjs.Nivel3Code.condition1IsTrue_1 = {val:false};
gdjs.Nivel3Code.condition2IsTrue_1 = {val:false};
gdjs.Nivel3Code.condition3IsTrue_1 = {val:false};


gdjs.Nivel3Code.eventsList0 = function(runtimeScene) {

{


gdjs.Nivel3Code.condition0IsTrue_0.val = false;
{
gdjs.Nivel3Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Nivel3Code.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Enemy"), gdjs.Nivel3Code.GDEnemyObjects2);
{for(var i = 0, len = gdjs.Nivel3Code.GDEnemyObjects2.length ;i < len;++i) {
    gdjs.Nivel3Code.GDEnemyObjects2[i].addPolarForce(-(45), gdjs.random(600) + 10, 1);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(3);
}}

}


{


gdjs.Nivel3Code.condition0IsTrue_0.val = false;
{
gdjs.Nivel3Code.condition0IsTrue_0.val = gdjs.evtTools.variable.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}if (gdjs.Nivel3Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Nivel1", false);
}}

}


};gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDPlayerObjects1Objects = Hashtable.newFrom({"Player": gdjs.Nivel3Code.GDPlayerObjects1});gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDEdgeObjects1Objects = Hashtable.newFrom({"Edge": gdjs.Nivel3Code.GDEdgeObjects1});gdjs.Nivel3Code.eventsList1 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Nivel3Code.GDPlayerObjects2);

gdjs.Nivel3Code.condition0IsTrue_0.val = false;
gdjs.Nivel3Code.condition1IsTrue_0.val = false;
{
{gdjs.Nivel3Code.conditionTrue_1 = gdjs.Nivel3Code.condition0IsTrue_0;
gdjs.Nivel3Code.condition0IsTrue_1.val = false;
gdjs.Nivel3Code.condition1IsTrue_1.val = false;
{
gdjs.Nivel3Code.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
if( gdjs.Nivel3Code.condition0IsTrue_1.val ) {
    gdjs.Nivel3Code.conditionTrue_1.val = true;
}
}
{
gdjs.Nivel3Code.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "a");
if( gdjs.Nivel3Code.condition1IsTrue_1.val ) {
    gdjs.Nivel3Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.Nivel3Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Nivel3Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Nivel3Code.GDPlayerObjects2[i].getX() > 0 ) {
        gdjs.Nivel3Code.condition1IsTrue_0.val = true;
        gdjs.Nivel3Code.GDPlayerObjects2[k] = gdjs.Nivel3Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Nivel3Code.GDPlayerObjects2.length = k;}}
if (gdjs.Nivel3Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Nivel3Code.GDPlayerObjects2 */
{}{for(var i = 0, len = gdjs.Nivel3Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Nivel3Code.GDPlayerObjects2[i].setX(gdjs.Nivel3Code.GDPlayerObjects2[i].getX() - (5));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Nivel3Code.GDPlayerObjects2);

gdjs.Nivel3Code.condition0IsTrue_0.val = false;
gdjs.Nivel3Code.condition1IsTrue_0.val = false;
{
{gdjs.Nivel3Code.conditionTrue_1 = gdjs.Nivel3Code.condition0IsTrue_0;
gdjs.Nivel3Code.condition0IsTrue_1.val = false;
gdjs.Nivel3Code.condition1IsTrue_1.val = false;
{
gdjs.Nivel3Code.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
if( gdjs.Nivel3Code.condition0IsTrue_1.val ) {
    gdjs.Nivel3Code.conditionTrue_1.val = true;
}
}
{
gdjs.Nivel3Code.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "d");
if( gdjs.Nivel3Code.condition1IsTrue_1.val ) {
    gdjs.Nivel3Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.Nivel3Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Nivel3Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Nivel3Code.GDPlayerObjects2[i].getX() < 800 - (gdjs.Nivel3Code.GDPlayerObjects2[i].getWidth()) ) {
        gdjs.Nivel3Code.condition1IsTrue_0.val = true;
        gdjs.Nivel3Code.GDPlayerObjects2[k] = gdjs.Nivel3Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Nivel3Code.GDPlayerObjects2.length = k;}}
if (gdjs.Nivel3Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Nivel3Code.GDPlayerObjects2 */
{}{for(var i = 0, len = gdjs.Nivel3Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Nivel3Code.GDPlayerObjects2[i].setX(gdjs.Nivel3Code.GDPlayerObjects2[i].getX() + (5));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Nivel3Code.GDPlayerObjects2);

gdjs.Nivel3Code.condition0IsTrue_0.val = false;
gdjs.Nivel3Code.condition1IsTrue_0.val = false;
{
{gdjs.Nivel3Code.conditionTrue_1 = gdjs.Nivel3Code.condition0IsTrue_0;
gdjs.Nivel3Code.condition0IsTrue_1.val = false;
gdjs.Nivel3Code.condition1IsTrue_1.val = false;
{
gdjs.Nivel3Code.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if( gdjs.Nivel3Code.condition0IsTrue_1.val ) {
    gdjs.Nivel3Code.conditionTrue_1.val = true;
}
}
{
gdjs.Nivel3Code.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "w");
if( gdjs.Nivel3Code.condition1IsTrue_1.val ) {
    gdjs.Nivel3Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.Nivel3Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Nivel3Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Nivel3Code.GDPlayerObjects2[i].getY() > 0 ) {
        gdjs.Nivel3Code.condition1IsTrue_0.val = true;
        gdjs.Nivel3Code.GDPlayerObjects2[k] = gdjs.Nivel3Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Nivel3Code.GDPlayerObjects2.length = k;}}
if (gdjs.Nivel3Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Nivel3Code.GDPlayerObjects2 */
{}{for(var i = 0, len = gdjs.Nivel3Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Nivel3Code.GDPlayerObjects2[i].setY(gdjs.Nivel3Code.GDPlayerObjects2[i].getY() - (5));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Nivel3Code.GDPlayerObjects2);

gdjs.Nivel3Code.condition0IsTrue_0.val = false;
gdjs.Nivel3Code.condition1IsTrue_0.val = false;
{
{gdjs.Nivel3Code.conditionTrue_1 = gdjs.Nivel3Code.condition0IsTrue_0;
gdjs.Nivel3Code.condition0IsTrue_1.val = false;
gdjs.Nivel3Code.condition1IsTrue_1.val = false;
{
gdjs.Nivel3Code.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Down");
if( gdjs.Nivel3Code.condition0IsTrue_1.val ) {
    gdjs.Nivel3Code.conditionTrue_1.val = true;
}
}
{
gdjs.Nivel3Code.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "s");
if( gdjs.Nivel3Code.condition1IsTrue_1.val ) {
    gdjs.Nivel3Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.Nivel3Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.Nivel3Code.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.Nivel3Code.GDPlayerObjects2[i].getY() < 600 - (gdjs.Nivel3Code.GDPlayerObjects2[i].getHeight()) ) {
        gdjs.Nivel3Code.condition1IsTrue_0.val = true;
        gdjs.Nivel3Code.GDPlayerObjects2[k] = gdjs.Nivel3Code.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.Nivel3Code.GDPlayerObjects2.length = k;}}
if (gdjs.Nivel3Code.condition1IsTrue_0.val) {
/* Reuse gdjs.Nivel3Code.GDPlayerObjects2 */
{}{for(var i = 0, len = gdjs.Nivel3Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Nivel3Code.GDPlayerObjects2[i].setY(gdjs.Nivel3Code.GDPlayerObjects2[i].getY() + (5));
}
}}

}


{


{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Nivel3Code.GDPlayerObjects2);
{for(var i = 0, len = gdjs.Nivel3Code.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Nivel3Code.GDPlayerObjects2[i].rotateTowardPosition(gdjs.evtTools.input.getMouseX(runtimeScene, "", 0), gdjs.evtTools.input.getMouseY(runtimeScene, "", 0), 0, runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Edge"), gdjs.Nivel3Code.GDEdgeObjects1);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Nivel3Code.GDPlayerObjects1);

gdjs.Nivel3Code.condition0IsTrue_0.val = false;
{
gdjs.Nivel3Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDPlayerObjects1Objects, gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDEdgeObjects1Objects, false, runtimeScene, false);
}if (gdjs.Nivel3Code.condition0IsTrue_0.val) {
}

}


};gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Nivel3Code.GDPlayerObjects2});gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDEdgeObjects2Objects = Hashtable.newFrom({"Edge": gdjs.Nivel3Code.GDEdgeObjects2});gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDBulletObjects2Objects = Hashtable.newFrom({"Bullet": gdjs.Nivel3Code.GDBulletObjects2});gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDBulletObjects2Objects = Hashtable.newFrom({"Bullet": gdjs.Nivel3Code.GDBulletObjects2});gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDEnemyObjects2Objects = Hashtable.newFrom({"Enemy": gdjs.Nivel3Code.GDEnemyObjects2});gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDBulletObjects1Objects = Hashtable.newFrom({"Bullet": gdjs.Nivel3Code.GDBulletObjects1});gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDEdgeObjects1Objects = Hashtable.newFrom({"Edge": gdjs.Nivel3Code.GDEdgeObjects1});gdjs.Nivel3Code.eventsList2 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Edge"), gdjs.Nivel3Code.GDEdgeObjects2);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Nivel3Code.GDPlayerObjects2);

gdjs.Nivel3Code.condition0IsTrue_0.val = false;
gdjs.Nivel3Code.condition1IsTrue_0.val = false;
gdjs.Nivel3Code.condition2IsTrue_0.val = false;
{
gdjs.Nivel3Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Nivel3Code.condition0IsTrue_0.val ) {
{
gdjs.Nivel3Code.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDPlayerObjects2Objects, gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDEdgeObjects2Objects, true, runtimeScene, false);
}if ( gdjs.Nivel3Code.condition1IsTrue_0.val ) {
{
{gdjs.Nivel3Code.conditionTrue_1 = gdjs.Nivel3Code.condition2IsTrue_0;
gdjs.Nivel3Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8433580);
}
}}
}
if (gdjs.Nivel3Code.condition2IsTrue_0.val) {
/* Reuse gdjs.Nivel3Code.GDPlayerObjects2 */
gdjs.Nivel3Code.GDBulletObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDBulletObjects2Objects, (( gdjs.Nivel3Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Nivel3Code.GDPlayerObjects2[0].getPointX("")) + ((( gdjs.Nivel3Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Nivel3Code.GDPlayerObjects2[0].getWidth()) / 2) + ((( gdjs.Nivel3Code.GDBulletObjects2.length === 0 ) ? 0 :gdjs.Nivel3Code.GDBulletObjects2[0].getWidth()) / 2), (( gdjs.Nivel3Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Nivel3Code.GDPlayerObjects2[0].getPointY("")) + ((( gdjs.Nivel3Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Nivel3Code.GDPlayerObjects2[0].getHeight()) / 2) + ((( gdjs.Nivel3Code.GDBulletObjects2.length === 0 ) ? 0 :gdjs.Nivel3Code.GDBulletObjects2[0].getHeight()) / 2), "");
}{for(var i = 0, len = gdjs.Nivel3Code.GDBulletObjects2.length ;i < len;++i) {
    gdjs.Nivel3Code.GDBulletObjects2[i].addPolarForce((( gdjs.Nivel3Code.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Nivel3Code.GDPlayerObjects2[0].getDirectionOrAngle()), 1000, 1);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Bullet"), gdjs.Nivel3Code.GDBulletObjects2);
gdjs.copyArray(runtimeScene.getObjects("Enemy"), gdjs.Nivel3Code.GDEnemyObjects2);

gdjs.Nivel3Code.condition0IsTrue_0.val = false;
{
gdjs.Nivel3Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDBulletObjects2Objects, gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDEnemyObjects2Objects, false, runtimeScene, false);
}if (gdjs.Nivel3Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Nivel3Code.GDBulletObjects2 */
/* Reuse gdjs.Nivel3Code.GDEnemyObjects2 */
{for(var i = 0, len = gdjs.Nivel3Code.GDBulletObjects2.length ;i < len;++i) {
    gdjs.Nivel3Code.GDBulletObjects2[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.Nivel3Code.GDEnemyObjects2.length ;i < len;++i) {
    gdjs.Nivel3Code.GDEnemyObjects2[i].deleteFromScene(runtimeScene);
}
}{runtimeScene.getGame().getVariables().getFromIndex(0).sub(1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Bullet"), gdjs.Nivel3Code.GDBulletObjects1);
gdjs.copyArray(runtimeScene.getObjects("Edge"), gdjs.Nivel3Code.GDEdgeObjects1);

gdjs.Nivel3Code.condition0IsTrue_0.val = false;
{
gdjs.Nivel3Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDBulletObjects1Objects, gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDEdgeObjects1Objects, false, runtimeScene, false);
}if (gdjs.Nivel3Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Nivel3Code.GDBulletObjects1 */
{for(var i = 0, len = gdjs.Nivel3Code.GDBulletObjects1.length ;i < len;++i) {
    gdjs.Nivel3Code.GDBulletObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDEnemyObjects2Objects = Hashtable.newFrom({"Enemy": gdjs.Nivel3Code.GDEnemyObjects2});gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDEdgeObjects2Objects = Hashtable.newFrom({"Edge": gdjs.Nivel3Code.GDEdgeObjects2});gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDEdgeObjects2Objects = Hashtable.newFrom({"Edge": gdjs.Nivel3Code.GDEdgeObjects2});gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDEnemyObjects1Objects = Hashtable.newFrom({"Enemy": gdjs.Nivel3Code.GDEnemyObjects1});gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDPlayerObjects1Objects = Hashtable.newFrom({"Player": gdjs.Nivel3Code.GDPlayerObjects1});gdjs.Nivel3Code.eventsList3 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Edge"), gdjs.Nivel3Code.GDEdgeObjects2);
gdjs.copyArray(runtimeScene.getObjects("Enemy"), gdjs.Nivel3Code.GDEnemyObjects2);

gdjs.Nivel3Code.condition0IsTrue_0.val = false;
{
gdjs.Nivel3Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDEnemyObjects2Objects, gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDEdgeObjects2Objects, false, runtimeScene, false);
}if (gdjs.Nivel3Code.condition0IsTrue_0.val) {
/* Reuse gdjs.Nivel3Code.GDEdgeObjects2 */
/* Reuse gdjs.Nivel3Code.GDEnemyObjects2 */
{for(var i = 0, len = gdjs.Nivel3Code.GDEnemyObjects2.length ;i < len;++i) {
    gdjs.Nivel3Code.GDEnemyObjects2[i].getBehavior("Bounce").BounceOff(gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDEdgeObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Enemy"), gdjs.Nivel3Code.GDEnemyObjects1);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Nivel3Code.GDPlayerObjects1);

gdjs.Nivel3Code.condition0IsTrue_0.val = false;
{
gdjs.Nivel3Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDEnemyObjects1Objects, gdjs.Nivel3Code.mapOfGDgdjs_46Nivel3Code_46GDPlayerObjects1Objects, false, runtimeScene, false);
}if (gdjs.Nivel3Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "Menu", false);
}}

}


};gdjs.Nivel3Code.eventsList4 = function(runtimeScene) {

{


gdjs.Nivel3Code.eventsList0(runtimeScene);
}


{


gdjs.Nivel3Code.eventsList1(runtimeScene);
}


{


gdjs.Nivel3Code.eventsList2(runtimeScene);
}


{


gdjs.Nivel3Code.eventsList3(runtimeScene);
}


};

gdjs.Nivel3Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.Nivel3Code.GDPlayerObjects1.length = 0;
gdjs.Nivel3Code.GDPlayerObjects2.length = 0;
gdjs.Nivel3Code.GDPlayerObjects3.length = 0;
gdjs.Nivel3Code.GDBulletObjects1.length = 0;
gdjs.Nivel3Code.GDBulletObjects2.length = 0;
gdjs.Nivel3Code.GDBulletObjects3.length = 0;
gdjs.Nivel3Code.GDEnemyObjects1.length = 0;
gdjs.Nivel3Code.GDEnemyObjects2.length = 0;
gdjs.Nivel3Code.GDEnemyObjects3.length = 0;
gdjs.Nivel3Code.GDEdgeObjects1.length = 0;
gdjs.Nivel3Code.GDEdgeObjects2.length = 0;
gdjs.Nivel3Code.GDEdgeObjects3.length = 0;

gdjs.Nivel3Code.eventsList4(runtimeScene);
return;

}

gdjs['Nivel3Code'] = gdjs.Nivel3Code;
