gdjs.Jogo_32abertoCode = {};
gdjs.Jogo_32abertoCode.GDPlayerObjects1= [];
gdjs.Jogo_32abertoCode.GDPlayerObjects2= [];
gdjs.Jogo_32abertoCode.GDPlayerObjects3= [];
gdjs.Jogo_32abertoCode.GDBulletObjects1= [];
gdjs.Jogo_32abertoCode.GDBulletObjects2= [];
gdjs.Jogo_32abertoCode.GDBulletObjects3= [];
gdjs.Jogo_32abertoCode.GDEnemyObjects1= [];
gdjs.Jogo_32abertoCode.GDEnemyObjects2= [];
gdjs.Jogo_32abertoCode.GDEnemyObjects3= [];
gdjs.Jogo_32abertoCode.GDEdgeObjects1= [];
gdjs.Jogo_32abertoCode.GDEdgeObjects2= [];
gdjs.Jogo_32abertoCode.GDEdgeObjects3= [];

gdjs.Jogo_32abertoCode.conditionTrue_0 = {val:false};
gdjs.Jogo_32abertoCode.condition0IsTrue_0 = {val:false};
gdjs.Jogo_32abertoCode.condition1IsTrue_0 = {val:false};
gdjs.Jogo_32abertoCode.condition2IsTrue_0 = {val:false};
gdjs.Jogo_32abertoCode.condition3IsTrue_0 = {val:false};
gdjs.Jogo_32abertoCode.conditionTrue_1 = {val:false};
gdjs.Jogo_32abertoCode.condition0IsTrue_1 = {val:false};
gdjs.Jogo_32abertoCode.condition1IsTrue_1 = {val:false};
gdjs.Jogo_32abertoCode.condition2IsTrue_1 = {val:false};
gdjs.Jogo_32abertoCode.condition3IsTrue_1 = {val:false};


gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEnemyObjects1Objects = Hashtable.newFrom({"Enemy": gdjs.Jogo_32abertoCode.GDEnemyObjects1});gdjs.Jogo_32abertoCode.eventsList0 = function(runtimeScene) {

{


gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = false;
{
gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.Jogo_32abertoCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Enemy"), gdjs.Jogo_32abertoCode.GDEnemyObjects2);
{for(var i = 0, len = gdjs.Jogo_32abertoCode.GDEnemyObjects2.length ;i < len;++i) {
    gdjs.Jogo_32abertoCode.GDEnemyObjects2[i].addPolarForce(-(45), gdjs.random(600) + 10, 1);
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "NovoInimigo");
}}

}


{


{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Jogo_32abertoCode.GDPlayerObjects2);
{gdjs.evtTools.camera.centerCamera(runtimeScene, (gdjs.Jogo_32abertoCode.GDPlayerObjects2.length !== 0 ? gdjs.Jogo_32abertoCode.GDPlayerObjects2[0] : null), true, "", 0);
}}

}


{


gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = false;
{
gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 1, "NovoInimigo");
}if (gdjs.Jogo_32abertoCode.condition0IsTrue_0.val) {
gdjs.Jogo_32abertoCode.GDEnemyObjects1.length = 0;

{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "NovoInimigo");
}{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEnemyObjects1Objects, gdjs.random(3500), gdjs.random(1200), "");
}{for(var i = 0, len = gdjs.Jogo_32abertoCode.GDEnemyObjects1.length ;i < len;++i) {
    gdjs.Jogo_32abertoCode.GDEnemyObjects1[i].addPolarForce(-(45), gdjs.random(600) + 10, 1);
}
}{runtimeScene.getVariables().getFromIndex(0).add(1);
}}

}


};gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDPlayerObjects1Objects = Hashtable.newFrom({"Player": gdjs.Jogo_32abertoCode.GDPlayerObjects1});gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEdgeObjects1Objects = Hashtable.newFrom({"Edge": gdjs.Jogo_32abertoCode.GDEdgeObjects1});gdjs.Jogo_32abertoCode.eventsList1 = function(runtimeScene) {

{


gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = false;
{
{gdjs.Jogo_32abertoCode.conditionTrue_1 = gdjs.Jogo_32abertoCode.condition0IsTrue_0;
gdjs.Jogo_32abertoCode.condition0IsTrue_1.val = false;
gdjs.Jogo_32abertoCode.condition1IsTrue_1.val = false;
{
gdjs.Jogo_32abertoCode.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
if( gdjs.Jogo_32abertoCode.condition0IsTrue_1.val ) {
    gdjs.Jogo_32abertoCode.conditionTrue_1.val = true;
}
}
{
gdjs.Jogo_32abertoCode.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "a");
if( gdjs.Jogo_32abertoCode.condition1IsTrue_1.val ) {
    gdjs.Jogo_32abertoCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.Jogo_32abertoCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Jogo_32abertoCode.GDPlayerObjects2);
{}{for(var i = 0, len = gdjs.Jogo_32abertoCode.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Jogo_32abertoCode.GDPlayerObjects2[i].setX(gdjs.Jogo_32abertoCode.GDPlayerObjects2[i].getX() - (5));
}
}}

}


{


gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = false;
{
{gdjs.Jogo_32abertoCode.conditionTrue_1 = gdjs.Jogo_32abertoCode.condition0IsTrue_0;
gdjs.Jogo_32abertoCode.condition0IsTrue_1.val = false;
gdjs.Jogo_32abertoCode.condition1IsTrue_1.val = false;
{
gdjs.Jogo_32abertoCode.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
if( gdjs.Jogo_32abertoCode.condition0IsTrue_1.val ) {
    gdjs.Jogo_32abertoCode.conditionTrue_1.val = true;
}
}
{
gdjs.Jogo_32abertoCode.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "d");
if( gdjs.Jogo_32abertoCode.condition1IsTrue_1.val ) {
    gdjs.Jogo_32abertoCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.Jogo_32abertoCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Jogo_32abertoCode.GDPlayerObjects2);
{}{for(var i = 0, len = gdjs.Jogo_32abertoCode.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Jogo_32abertoCode.GDPlayerObjects2[i].setX(gdjs.Jogo_32abertoCode.GDPlayerObjects2[i].getX() + (5));
}
}}

}


{


gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = false;
{
{gdjs.Jogo_32abertoCode.conditionTrue_1 = gdjs.Jogo_32abertoCode.condition0IsTrue_0;
gdjs.Jogo_32abertoCode.condition0IsTrue_1.val = false;
gdjs.Jogo_32abertoCode.condition1IsTrue_1.val = false;
{
gdjs.Jogo_32abertoCode.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if( gdjs.Jogo_32abertoCode.condition0IsTrue_1.val ) {
    gdjs.Jogo_32abertoCode.conditionTrue_1.val = true;
}
}
{
gdjs.Jogo_32abertoCode.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "w");
if( gdjs.Jogo_32abertoCode.condition1IsTrue_1.val ) {
    gdjs.Jogo_32abertoCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.Jogo_32abertoCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Jogo_32abertoCode.GDPlayerObjects2);
{}{for(var i = 0, len = gdjs.Jogo_32abertoCode.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Jogo_32abertoCode.GDPlayerObjects2[i].setY(gdjs.Jogo_32abertoCode.GDPlayerObjects2[i].getY() - (5));
}
}}

}


{


gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = false;
{
{gdjs.Jogo_32abertoCode.conditionTrue_1 = gdjs.Jogo_32abertoCode.condition0IsTrue_0;
gdjs.Jogo_32abertoCode.condition0IsTrue_1.val = false;
gdjs.Jogo_32abertoCode.condition1IsTrue_1.val = false;
{
gdjs.Jogo_32abertoCode.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Down");
if( gdjs.Jogo_32abertoCode.condition0IsTrue_1.val ) {
    gdjs.Jogo_32abertoCode.conditionTrue_1.val = true;
}
}
{
gdjs.Jogo_32abertoCode.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "s");
if( gdjs.Jogo_32abertoCode.condition1IsTrue_1.val ) {
    gdjs.Jogo_32abertoCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.Jogo_32abertoCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Jogo_32abertoCode.GDPlayerObjects2);
{}{for(var i = 0, len = gdjs.Jogo_32abertoCode.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Jogo_32abertoCode.GDPlayerObjects2[i].setY(gdjs.Jogo_32abertoCode.GDPlayerObjects2[i].getY() + (5));
}
}}

}


{


{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Jogo_32abertoCode.GDPlayerObjects2);
{for(var i = 0, len = gdjs.Jogo_32abertoCode.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.Jogo_32abertoCode.GDPlayerObjects2[i].rotateTowardPosition(gdjs.evtTools.input.getMouseX(runtimeScene, "", 0), gdjs.evtTools.input.getMouseY(runtimeScene, "", 0), 0, runtimeScene);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Edge"), gdjs.Jogo_32abertoCode.GDEdgeObjects1);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Jogo_32abertoCode.GDPlayerObjects1);

gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = false;
{
gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDPlayerObjects1Objects, gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEdgeObjects1Objects, false, runtimeScene, false);
}if (gdjs.Jogo_32abertoCode.condition0IsTrue_0.val) {
}

}


};gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDPlayerObjects2Objects = Hashtable.newFrom({"Player": gdjs.Jogo_32abertoCode.GDPlayerObjects2});gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEdgeObjects2Objects = Hashtable.newFrom({"Edge": gdjs.Jogo_32abertoCode.GDEdgeObjects2});gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDBulletObjects2Objects = Hashtable.newFrom({"Bullet": gdjs.Jogo_32abertoCode.GDBulletObjects2});gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDBulletObjects2Objects = Hashtable.newFrom({"Bullet": gdjs.Jogo_32abertoCode.GDBulletObjects2});gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEnemyObjects2Objects = Hashtable.newFrom({"Enemy": gdjs.Jogo_32abertoCode.GDEnemyObjects2});gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDBulletObjects1Objects = Hashtable.newFrom({"Bullet": gdjs.Jogo_32abertoCode.GDBulletObjects1});gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEdgeObjects1Objects = Hashtable.newFrom({"Edge": gdjs.Jogo_32abertoCode.GDEdgeObjects1});gdjs.Jogo_32abertoCode.eventsList2 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Edge"), gdjs.Jogo_32abertoCode.GDEdgeObjects2);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Jogo_32abertoCode.GDPlayerObjects2);

gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = false;
gdjs.Jogo_32abertoCode.condition1IsTrue_0.val = false;
gdjs.Jogo_32abertoCode.condition2IsTrue_0.val = false;
{
gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if ( gdjs.Jogo_32abertoCode.condition0IsTrue_0.val ) {
{
gdjs.Jogo_32abertoCode.condition1IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDPlayerObjects2Objects, gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEdgeObjects2Objects, true, runtimeScene, false);
}if ( gdjs.Jogo_32abertoCode.condition1IsTrue_0.val ) {
{
{gdjs.Jogo_32abertoCode.conditionTrue_1 = gdjs.Jogo_32abertoCode.condition2IsTrue_0;
gdjs.Jogo_32abertoCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(8450268);
}
}}
}
if (gdjs.Jogo_32abertoCode.condition2IsTrue_0.val) {
/* Reuse gdjs.Jogo_32abertoCode.GDPlayerObjects2 */
gdjs.Jogo_32abertoCode.GDBulletObjects2.length = 0;

{gdjs.evtTools.object.createObjectOnScene((typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene), gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDBulletObjects2Objects, (( gdjs.Jogo_32abertoCode.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Jogo_32abertoCode.GDPlayerObjects2[0].getPointX("")) + ((( gdjs.Jogo_32abertoCode.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Jogo_32abertoCode.GDPlayerObjects2[0].getWidth()) / 2) + ((( gdjs.Jogo_32abertoCode.GDBulletObjects2.length === 0 ) ? 0 :gdjs.Jogo_32abertoCode.GDBulletObjects2[0].getWidth()) / 2), (( gdjs.Jogo_32abertoCode.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Jogo_32abertoCode.GDPlayerObjects2[0].getPointY("")) + ((( gdjs.Jogo_32abertoCode.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Jogo_32abertoCode.GDPlayerObjects2[0].getHeight()) / 2) + ((( gdjs.Jogo_32abertoCode.GDBulletObjects2.length === 0 ) ? 0 :gdjs.Jogo_32abertoCode.GDBulletObjects2[0].getHeight()) / 2), "");
}{for(var i = 0, len = gdjs.Jogo_32abertoCode.GDBulletObjects2.length ;i < len;++i) {
    gdjs.Jogo_32abertoCode.GDBulletObjects2[i].addPolarForce((( gdjs.Jogo_32abertoCode.GDPlayerObjects2.length === 0 ) ? 0 :gdjs.Jogo_32abertoCode.GDPlayerObjects2[0].getDirectionOrAngle()), 1000, 1);
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Bullet"), gdjs.Jogo_32abertoCode.GDBulletObjects2);
gdjs.copyArray(runtimeScene.getObjects("Enemy"), gdjs.Jogo_32abertoCode.GDEnemyObjects2);

gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = false;
{
gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDBulletObjects2Objects, gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEnemyObjects2Objects, false, runtimeScene, false);
}if (gdjs.Jogo_32abertoCode.condition0IsTrue_0.val) {
/* Reuse gdjs.Jogo_32abertoCode.GDBulletObjects2 */
/* Reuse gdjs.Jogo_32abertoCode.GDEnemyObjects2 */
{for(var i = 0, len = gdjs.Jogo_32abertoCode.GDBulletObjects2.length ;i < len;++i) {
    gdjs.Jogo_32abertoCode.GDBulletObjects2[i].deleteFromScene(runtimeScene);
}
}{for(var i = 0, len = gdjs.Jogo_32abertoCode.GDEnemyObjects2.length ;i < len;++i) {
    gdjs.Jogo_32abertoCode.GDEnemyObjects2[i].deleteFromScene(runtimeScene);
}
}{runtimeScene.getVariables().getFromIndex(0).sub(1);
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Bullet"), gdjs.Jogo_32abertoCode.GDBulletObjects1);
gdjs.copyArray(runtimeScene.getObjects("Edge"), gdjs.Jogo_32abertoCode.GDEdgeObjects1);

gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = false;
{
gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDBulletObjects1Objects, gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEdgeObjects1Objects, false, runtimeScene, false);
}if (gdjs.Jogo_32abertoCode.condition0IsTrue_0.val) {
/* Reuse gdjs.Jogo_32abertoCode.GDBulletObjects1 */
{for(var i = 0, len = gdjs.Jogo_32abertoCode.GDBulletObjects1.length ;i < len;++i) {
    gdjs.Jogo_32abertoCode.GDBulletObjects1[i].deleteFromScene(runtimeScene);
}
}}

}


};gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEnemyObjects2Objects = Hashtable.newFrom({"Enemy": gdjs.Jogo_32abertoCode.GDEnemyObjects2});gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEdgeObjects2Objects = Hashtable.newFrom({"Edge": gdjs.Jogo_32abertoCode.GDEdgeObjects2});gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEdgeObjects2Objects = Hashtable.newFrom({"Edge": gdjs.Jogo_32abertoCode.GDEdgeObjects2});gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEnemyObjects1Objects = Hashtable.newFrom({"Enemy": gdjs.Jogo_32abertoCode.GDEnemyObjects1});gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDPlayerObjects1Objects = Hashtable.newFrom({"Player": gdjs.Jogo_32abertoCode.GDPlayerObjects1});gdjs.Jogo_32abertoCode.eventsList3 = function(runtimeScene) {

{

gdjs.copyArray(runtimeScene.getObjects("Edge"), gdjs.Jogo_32abertoCode.GDEdgeObjects2);
gdjs.copyArray(runtimeScene.getObjects("Enemy"), gdjs.Jogo_32abertoCode.GDEnemyObjects2);

gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = false;
{
gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEnemyObjects2Objects, gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEdgeObjects2Objects, false, runtimeScene, false);
}if (gdjs.Jogo_32abertoCode.condition0IsTrue_0.val) {
/* Reuse gdjs.Jogo_32abertoCode.GDEdgeObjects2 */
/* Reuse gdjs.Jogo_32abertoCode.GDEnemyObjects2 */
{for(var i = 0, len = gdjs.Jogo_32abertoCode.GDEnemyObjects2.length ;i < len;++i) {
    gdjs.Jogo_32abertoCode.GDEnemyObjects2[i].getBehavior("Bounce").BounceOff(gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEdgeObjects2Objects, (typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : undefined));
}
}}

}


{

gdjs.copyArray(runtimeScene.getObjects("Enemy"), gdjs.Jogo_32abertoCode.GDEnemyObjects1);
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.Jogo_32abertoCode.GDPlayerObjects1);

gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = false;
{
gdjs.Jogo_32abertoCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDEnemyObjects1Objects, gdjs.Jogo_32abertoCode.mapOfGDgdjs_46Jogo_9532abertoCode_46GDPlayerObjects1Objects, false, runtimeScene, false);
}if (gdjs.Jogo_32abertoCode.condition0IsTrue_0.val) {
/* Reuse gdjs.Jogo_32abertoCode.GDPlayerObjects1 */
{for(var i = 0, len = gdjs.Jogo_32abertoCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Jogo_32abertoCode.GDPlayerObjects1[i].setX(0);
}
}{for(var i = 0, len = gdjs.Jogo_32abertoCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.Jogo_32abertoCode.GDPlayerObjects1[i].setY(0);
}
}}

}


};gdjs.Jogo_32abertoCode.eventsList4 = function(runtimeScene) {

{


gdjs.Jogo_32abertoCode.eventsList0(runtimeScene);
}


{


gdjs.Jogo_32abertoCode.eventsList1(runtimeScene);
}


{


gdjs.Jogo_32abertoCode.eventsList2(runtimeScene);
}


{


gdjs.Jogo_32abertoCode.eventsList3(runtimeScene);
}


};

gdjs.Jogo_32abertoCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.Jogo_32abertoCode.GDPlayerObjects1.length = 0;
gdjs.Jogo_32abertoCode.GDPlayerObjects2.length = 0;
gdjs.Jogo_32abertoCode.GDPlayerObjects3.length = 0;
gdjs.Jogo_32abertoCode.GDBulletObjects1.length = 0;
gdjs.Jogo_32abertoCode.GDBulletObjects2.length = 0;
gdjs.Jogo_32abertoCode.GDBulletObjects3.length = 0;
gdjs.Jogo_32abertoCode.GDEnemyObjects1.length = 0;
gdjs.Jogo_32abertoCode.GDEnemyObjects2.length = 0;
gdjs.Jogo_32abertoCode.GDEnemyObjects3.length = 0;
gdjs.Jogo_32abertoCode.GDEdgeObjects1.length = 0;
gdjs.Jogo_32abertoCode.GDEdgeObjects2.length = 0;
gdjs.Jogo_32abertoCode.GDEdgeObjects3.length = 0;

gdjs.Jogo_32abertoCode.eventsList4(runtimeScene);
return;

}

gdjs['Jogo_32abertoCode'] = gdjs.Jogo_32abertoCode;
