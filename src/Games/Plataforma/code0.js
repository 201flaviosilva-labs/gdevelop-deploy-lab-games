gdjs.PrimeiraCenaCode = {};
gdjs.PrimeiraCenaCode.GDPlayerObjects2_1final = [];

gdjs.PrimeiraCenaCode.GDPlayerObjects1= [];
gdjs.PrimeiraCenaCode.GDPlayerObjects2= [];
gdjs.PrimeiraCenaCode.GDPlayerObjects3= [];
gdjs.PrimeiraCenaCode.GDSolidoObjects1= [];
gdjs.PrimeiraCenaCode.GDSolidoObjects2= [];
gdjs.PrimeiraCenaCode.GDSolidoObjects3= [];

gdjs.PrimeiraCenaCode.conditionTrue_0 = {val:false};
gdjs.PrimeiraCenaCode.condition0IsTrue_0 = {val:false};
gdjs.PrimeiraCenaCode.condition1IsTrue_0 = {val:false};
gdjs.PrimeiraCenaCode.condition2IsTrue_0 = {val:false};
gdjs.PrimeiraCenaCode.condition3IsTrue_0 = {val:false};
gdjs.PrimeiraCenaCode.conditionTrue_1 = {val:false};
gdjs.PrimeiraCenaCode.condition0IsTrue_1 = {val:false};
gdjs.PrimeiraCenaCode.condition1IsTrue_1 = {val:false};
gdjs.PrimeiraCenaCode.condition2IsTrue_1 = {val:false};
gdjs.PrimeiraCenaCode.condition3IsTrue_1 = {val:false};


gdjs.PrimeiraCenaCode.eventsList0 = function(runtimeScene) {

{


gdjs.PrimeiraCenaCode.condition0IsTrue_0.val = false;
gdjs.PrimeiraCenaCode.condition1IsTrue_0.val = false;
{
{gdjs.PrimeiraCenaCode.conditionTrue_1 = gdjs.PrimeiraCenaCode.condition0IsTrue_0;
gdjs.PrimeiraCenaCode.condition0IsTrue_1.val = false;
gdjs.PrimeiraCenaCode.condition1IsTrue_1.val = false;
{
gdjs.PrimeiraCenaCode.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Space");
if( gdjs.PrimeiraCenaCode.condition0IsTrue_1.val ) {
    gdjs.PrimeiraCenaCode.conditionTrue_1.val = true;
}
}
{
gdjs.PrimeiraCenaCode.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if( gdjs.PrimeiraCenaCode.condition1IsTrue_1.val ) {
    gdjs.PrimeiraCenaCode.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.PrimeiraCenaCode.condition0IsTrue_0.val ) {
{
{gdjs.PrimeiraCenaCode.conditionTrue_1 = gdjs.PrimeiraCenaCode.condition1IsTrue_0;
gdjs.PrimeiraCenaCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7194332);
}
}}
if (gdjs.PrimeiraCenaCode.condition1IsTrue_0.val) {
/* Reuse gdjs.PrimeiraCenaCode.GDPlayerObjects2 */
{for(var i = 0, len = gdjs.PrimeiraCenaCode.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.PrimeiraCenaCode.GDPlayerObjects2[i].getBehavior("PlatformerObject").setCanJump();
}
}{for(var i = 0, len = gdjs.PrimeiraCenaCode.GDPlayerObjects2.length ;i < len;++i) {
    gdjs.PrimeiraCenaCode.GDPlayerObjects2[i].returnVariable(gdjs.PrimeiraCenaCode.GDPlayerObjects2[i].getVariables().getFromIndex(1)).add(1);
}
}}

}


};gdjs.PrimeiraCenaCode.eventsList1 = function(runtimeScene) {

{

gdjs.copyArray(gdjs.PrimeiraCenaCode.GDPlayerObjects1, gdjs.PrimeiraCenaCode.GDPlayerObjects2);


gdjs.PrimeiraCenaCode.condition0IsTrue_0.val = false;
gdjs.PrimeiraCenaCode.condition1IsTrue_0.val = false;
{
{gdjs.PrimeiraCenaCode.conditionTrue_1 = gdjs.PrimeiraCenaCode.condition0IsTrue_0;
gdjs.PrimeiraCenaCode.GDPlayerObjects2_1final.length = 0;gdjs.PrimeiraCenaCode.condition0IsTrue_1.val = false;
gdjs.PrimeiraCenaCode.condition1IsTrue_1.val = false;
{
gdjs.copyArray(gdjs.PrimeiraCenaCode.GDPlayerObjects1, gdjs.PrimeiraCenaCode.GDPlayerObjects3);

for(var i = 0, k = 0, l = gdjs.PrimeiraCenaCode.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.PrimeiraCenaCode.GDPlayerObjects3[i].getBehavior("PlatformerObject").isFalling() ) {
        gdjs.PrimeiraCenaCode.condition0IsTrue_1.val = true;
        gdjs.PrimeiraCenaCode.GDPlayerObjects3[k] = gdjs.PrimeiraCenaCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.PrimeiraCenaCode.GDPlayerObjects3.length = k;if( gdjs.PrimeiraCenaCode.condition0IsTrue_1.val ) {
    gdjs.PrimeiraCenaCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.PrimeiraCenaCode.GDPlayerObjects3.length;j<jLen;++j) {
        if ( gdjs.PrimeiraCenaCode.GDPlayerObjects2_1final.indexOf(gdjs.PrimeiraCenaCode.GDPlayerObjects3[j]) === -1 )
            gdjs.PrimeiraCenaCode.GDPlayerObjects2_1final.push(gdjs.PrimeiraCenaCode.GDPlayerObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.PrimeiraCenaCode.GDPlayerObjects1, gdjs.PrimeiraCenaCode.GDPlayerObjects3);

for(var i = 0, k = 0, l = gdjs.PrimeiraCenaCode.GDPlayerObjects3.length;i<l;++i) {
    if ( gdjs.PrimeiraCenaCode.GDPlayerObjects3[i].getBehavior("PlatformerObject").isJumping() ) {
        gdjs.PrimeiraCenaCode.condition1IsTrue_1.val = true;
        gdjs.PrimeiraCenaCode.GDPlayerObjects3[k] = gdjs.PrimeiraCenaCode.GDPlayerObjects3[i];
        ++k;
    }
}
gdjs.PrimeiraCenaCode.GDPlayerObjects3.length = k;if( gdjs.PrimeiraCenaCode.condition1IsTrue_1.val ) {
    gdjs.PrimeiraCenaCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.PrimeiraCenaCode.GDPlayerObjects3.length;j<jLen;++j) {
        if ( gdjs.PrimeiraCenaCode.GDPlayerObjects2_1final.indexOf(gdjs.PrimeiraCenaCode.GDPlayerObjects3[j]) === -1 )
            gdjs.PrimeiraCenaCode.GDPlayerObjects2_1final.push(gdjs.PrimeiraCenaCode.GDPlayerObjects3[j]);
    }
}
}
{
gdjs.copyArray(gdjs.PrimeiraCenaCode.GDPlayerObjects2_1final, gdjs.PrimeiraCenaCode.GDPlayerObjects2);
}
}
}if ( gdjs.PrimeiraCenaCode.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.PrimeiraCenaCode.GDPlayerObjects2.length;i<l;++i) {
    if ( gdjs.PrimeiraCenaCode.GDPlayerObjects2[i].getVariableNumber(gdjs.PrimeiraCenaCode.GDPlayerObjects2[i].getVariables().getFromIndex(1)) < 2 ) {
        gdjs.PrimeiraCenaCode.condition1IsTrue_0.val = true;
        gdjs.PrimeiraCenaCode.GDPlayerObjects2[k] = gdjs.PrimeiraCenaCode.GDPlayerObjects2[i];
        ++k;
    }
}
gdjs.PrimeiraCenaCode.GDPlayerObjects2.length = k;}}
if (gdjs.PrimeiraCenaCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.PrimeiraCenaCode.eventsList0(runtimeScene);} //End of subevents
}

}


{

/* Reuse gdjs.PrimeiraCenaCode.GDPlayerObjects1 */

gdjs.PrimeiraCenaCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.PrimeiraCenaCode.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.PrimeiraCenaCode.GDPlayerObjects1[i].getBehavior("PlatformerObject").isOnFloor() ) {
        gdjs.PrimeiraCenaCode.condition0IsTrue_0.val = true;
        gdjs.PrimeiraCenaCode.GDPlayerObjects1[k] = gdjs.PrimeiraCenaCode.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.PrimeiraCenaCode.GDPlayerObjects1.length = k;}if (gdjs.PrimeiraCenaCode.condition0IsTrue_0.val) {
/* Reuse gdjs.PrimeiraCenaCode.GDPlayerObjects1 */
{for(var i = 0, len = gdjs.PrimeiraCenaCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.PrimeiraCenaCode.GDPlayerObjects1[i].returnVariable(gdjs.PrimeiraCenaCode.GDPlayerObjects1[i].getVariables().getFromIndex(1)).setNumber(0);
}
}}

}


};gdjs.PrimeiraCenaCode.eventsList2 = function(runtimeScene) {

{



}


{

gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.PrimeiraCenaCode.GDPlayerObjects1);

gdjs.PrimeiraCenaCode.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.PrimeiraCenaCode.GDPlayerObjects1.length;i<l;++i) {
    if ( gdjs.PrimeiraCenaCode.GDPlayerObjects1[i].getVariableNumber(gdjs.PrimeiraCenaCode.GDPlayerObjects1[i].getVariables().getFromIndex(0)) == 1 ) {
        gdjs.PrimeiraCenaCode.condition0IsTrue_0.val = true;
        gdjs.PrimeiraCenaCode.GDPlayerObjects1[k] = gdjs.PrimeiraCenaCode.GDPlayerObjects1[i];
        ++k;
    }
}
gdjs.PrimeiraCenaCode.GDPlayerObjects1.length = k;}if (gdjs.PrimeiraCenaCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.PrimeiraCenaCode.eventsList1(runtimeScene);} //End of subevents
}

}


};gdjs.PrimeiraCenaCode.eventsList3 = function(runtimeScene) {

{



}


{


gdjs.PrimeiraCenaCode.condition0IsTrue_0.val = false;
{
{gdjs.PrimeiraCenaCode.conditionTrue_1 = gdjs.PrimeiraCenaCode.condition0IsTrue_0;
gdjs.PrimeiraCenaCode.condition0IsTrue_1.val = false;
gdjs.PrimeiraCenaCode.condition1IsTrue_1.val = false;
gdjs.PrimeiraCenaCode.condition2IsTrue_1.val = false;
{
gdjs.PrimeiraCenaCode.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Up");
if( gdjs.PrimeiraCenaCode.condition0IsTrue_1.val ) {
    gdjs.PrimeiraCenaCode.conditionTrue_1.val = true;
}
}
{
gdjs.PrimeiraCenaCode.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "w");
if( gdjs.PrimeiraCenaCode.condition1IsTrue_1.val ) {
    gdjs.PrimeiraCenaCode.conditionTrue_1.val = true;
}
}
{
gdjs.PrimeiraCenaCode.condition2IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Space");
if( gdjs.PrimeiraCenaCode.condition2IsTrue_1.val ) {
    gdjs.PrimeiraCenaCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.PrimeiraCenaCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.PrimeiraCenaCode.GDPlayerObjects1);
{for(var i = 0, len = gdjs.PrimeiraCenaCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.PrimeiraCenaCode.GDPlayerObjects1[i].getBehavior("PlatformerObject").simulateJumpKey();
}
}}

}


{


gdjs.PrimeiraCenaCode.condition0IsTrue_0.val = false;
{
{gdjs.PrimeiraCenaCode.conditionTrue_1 = gdjs.PrimeiraCenaCode.condition0IsTrue_0;
gdjs.PrimeiraCenaCode.condition0IsTrue_1.val = false;
gdjs.PrimeiraCenaCode.condition1IsTrue_1.val = false;
{
gdjs.PrimeiraCenaCode.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "a");
if( gdjs.PrimeiraCenaCode.condition0IsTrue_1.val ) {
    gdjs.PrimeiraCenaCode.conditionTrue_1.val = true;
}
}
{
gdjs.PrimeiraCenaCode.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Left");
if( gdjs.PrimeiraCenaCode.condition1IsTrue_1.val ) {
    gdjs.PrimeiraCenaCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.PrimeiraCenaCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.PrimeiraCenaCode.GDPlayerObjects1);
{for(var i = 0, len = gdjs.PrimeiraCenaCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.PrimeiraCenaCode.GDPlayerObjects1[i].getBehavior("PlatformerObject").simulateLeftKey();
}
}{for(var i = 0, len = gdjs.PrimeiraCenaCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.PrimeiraCenaCode.GDPlayerObjects1[i].flipX(true);
}
}}

}


{


gdjs.PrimeiraCenaCode.condition0IsTrue_0.val = false;
{
{gdjs.PrimeiraCenaCode.conditionTrue_1 = gdjs.PrimeiraCenaCode.condition0IsTrue_0;
gdjs.PrimeiraCenaCode.condition0IsTrue_1.val = false;
gdjs.PrimeiraCenaCode.condition1IsTrue_1.val = false;
{
gdjs.PrimeiraCenaCode.condition0IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "d");
if( gdjs.PrimeiraCenaCode.condition0IsTrue_1.val ) {
    gdjs.PrimeiraCenaCode.conditionTrue_1.val = true;
}
}
{
gdjs.PrimeiraCenaCode.condition1IsTrue_1.val = gdjs.evtTools.input.isKeyPressed(runtimeScene, "Right");
if( gdjs.PrimeiraCenaCode.condition1IsTrue_1.val ) {
    gdjs.PrimeiraCenaCode.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.PrimeiraCenaCode.condition0IsTrue_0.val) {
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.PrimeiraCenaCode.GDPlayerObjects1);
{for(var i = 0, len = gdjs.PrimeiraCenaCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.PrimeiraCenaCode.GDPlayerObjects1[i].getBehavior("PlatformerObject").simulateRightKey();
}
}{for(var i = 0, len = gdjs.PrimeiraCenaCode.GDPlayerObjects1.length ;i < len;++i) {
    gdjs.PrimeiraCenaCode.GDPlayerObjects1[i].flipX(false);
}
}}

}


{



}


{


gdjs.PrimeiraCenaCode.eventsList2(runtimeScene);
}


{


{
gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.PrimeiraCenaCode.GDPlayerObjects1);
{gdjs.evtTools.camera.centerCamera(runtimeScene, (gdjs.PrimeiraCenaCode.GDPlayerObjects1.length !== 0 ? gdjs.PrimeiraCenaCode.GDPlayerObjects1[0] : null), true, "", 0);
}}

}


};

gdjs.PrimeiraCenaCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.PrimeiraCenaCode.GDPlayerObjects1.length = 0;
gdjs.PrimeiraCenaCode.GDPlayerObjects2.length = 0;
gdjs.PrimeiraCenaCode.GDPlayerObjects3.length = 0;
gdjs.PrimeiraCenaCode.GDSolidoObjects1.length = 0;
gdjs.PrimeiraCenaCode.GDSolidoObjects2.length = 0;
gdjs.PrimeiraCenaCode.GDSolidoObjects3.length = 0;

gdjs.PrimeiraCenaCode.eventsList3(runtimeScene);
return;

}

gdjs['PrimeiraCenaCode'] = gdjs.PrimeiraCenaCode;
