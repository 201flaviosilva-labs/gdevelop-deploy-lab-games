"use strict";

gdjs.MapaCode = {};
gdjs.MapaCode.GDInimigoObjects1_1final = [];
gdjs.MapaCode.GDParedeObjects1 = [];
gdjs.MapaCode.GDParedeObjects2 = [];
gdjs.MapaCode.GDPlayerObjects1 = [];
gdjs.MapaCode.GDPlayerObjects2 = [];
gdjs.MapaCode.GDCoinObjects1 = [];
gdjs.MapaCode.GDCoinObjects2 = [];
gdjs.MapaCode.GDDeadLineObjects1 = [];
gdjs.MapaCode.GDDeadLineObjects2 = [];
gdjs.MapaCode.GDInimigoObjects1 = [];
gdjs.MapaCode.GDInimigoObjects2 = [];
gdjs.MapaCode.conditionTrue_0 = {
  val: false
};
gdjs.MapaCode.condition0IsTrue_0 = {
  val: false
};
gdjs.MapaCode.condition1IsTrue_0 = {
  val: false
};
gdjs.MapaCode.condition2IsTrue_0 = {
  val: false
};
gdjs.MapaCode.condition3IsTrue_0 = {
  val: false
};
gdjs.MapaCode.conditionTrue_1 = {
  val: false
};
gdjs.MapaCode.condition0IsTrue_1 = {
  val: false
};
gdjs.MapaCode.condition1IsTrue_1 = {
  val: false
};
gdjs.MapaCode.condition2IsTrue_1 = {
  val: false
};
gdjs.MapaCode.condition3IsTrue_1 = {
  val: false
};
gdjs.MapaCode.mapOfGDgdjs_46MapaCode_46GDPlayerObjects1Objects = Hashtable.newFrom({
  "Player": gdjs.MapaCode.GDPlayerObjects1
});
gdjs.MapaCode.mapOfGDgdjs_46MapaCode_46GDInimigoObjects1Objects = Hashtable.newFrom({
  "Inimigo": gdjs.MapaCode.GDInimigoObjects1
});
gdjs.MapaCode.mapOfGDgdjs_46MapaCode_46GDPlayerObjects1Objects = Hashtable.newFrom({
  "Player": gdjs.MapaCode.GDPlayerObjects1
});
gdjs.MapaCode.mapOfGDgdjs_46MapaCode_46GDCoinObjects1Objects = Hashtable.newFrom({
  "Coin": gdjs.MapaCode.GDCoinObjects1
});
gdjs.MapaCode.mapOfGDgdjs_46MapaCode_46GDCoinObjects1Objects = Hashtable.newFrom({
  "Coin": gdjs.MapaCode.GDCoinObjects1
});
gdjs.MapaCode.mapOfGDgdjs_46MapaCode_46GDPlayerObjects1Objects = Hashtable.newFrom({
  "Player": gdjs.MapaCode.GDPlayerObjects1
});
gdjs.MapaCode.mapOfGDgdjs_46MapaCode_46GDDeadLineObjects1Objects = Hashtable.newFrom({
  "DeadLine": gdjs.MapaCode.GDDeadLineObjects1
});

gdjs.MapaCode.eventsList0 = function (runtimeScene) {
  {
    gdjs.copyArray(runtimeScene.getObjects("Inimigo"), gdjs.MapaCode.GDInimigoObjects1);
    gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MapaCode.GDPlayerObjects1);
    gdjs.MapaCode.condition0IsTrue_0.val = false;
    {
      gdjs.MapaCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MapaCode.mapOfGDgdjs_46MapaCode_46GDPlayerObjects1Objects, gdjs.MapaCode.mapOfGDgdjs_46MapaCode_46GDInimigoObjects1Objects, false, runtimeScene, false);
    }

    if (gdjs.MapaCode.condition0IsTrue_0.val) {
      /* Reuse gdjs.MapaCode.GDInimigoObjects1 */
      {
        for (var i = 0, len = gdjs.MapaCode.GDInimigoObjects1.length; i < len; ++i) {
          gdjs.MapaCode.GDInimigoObjects1[i].deleteFromScene(runtimeScene);
        }
      }
    }
  }
  {
    gdjs.copyArray(runtimeScene.getObjects("Coin"), gdjs.MapaCode.GDCoinObjects1);
    gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MapaCode.GDPlayerObjects1);
    gdjs.MapaCode.condition0IsTrue_0.val = false;
    {
      gdjs.MapaCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MapaCode.mapOfGDgdjs_46MapaCode_46GDPlayerObjects1Objects, gdjs.MapaCode.mapOfGDgdjs_46MapaCode_46GDCoinObjects1Objects, false, runtimeScene, false);
    }

    if (gdjs.MapaCode.condition0IsTrue_0.val) {
      /* Reuse gdjs.MapaCode.GDCoinObjects1 */
      {
        for (var i = 0, len = gdjs.MapaCode.GDCoinObjects1.length; i < len; ++i) {
          gdjs.MapaCode.GDCoinObjects1[i].deleteFromScene(runtimeScene);
        }
      }
      {
        gdjs.evtTools.object.createObjectOnScene(typeof eventsFunctionContext !== 'undefined' ? eventsFunctionContext : runtimeScene, gdjs.MapaCode.mapOfGDgdjs_46MapaCode_46GDCoinObjects1Objects, gdjs.random(760) + 16, gdjs.random(560) + 16, "");
      }
    }
  }
  {
    gdjs.copyArray(runtimeScene.getObjects("DeadLine"), gdjs.MapaCode.GDDeadLineObjects1);
    gdjs.copyArray(runtimeScene.getObjects("Player"), gdjs.MapaCode.GDPlayerObjects1);
    gdjs.MapaCode.condition0IsTrue_0.val = false;
    {
      gdjs.MapaCode.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.MapaCode.mapOfGDgdjs_46MapaCode_46GDPlayerObjects1Objects, gdjs.MapaCode.mapOfGDgdjs_46MapaCode_46GDDeadLineObjects1Objects, false, runtimeScene, false);
    }

    if (gdjs.MapaCode.condition0IsTrue_0.val) {
      /* Reuse gdjs.MapaCode.GDPlayerObjects1 */
      {
        for (var i = 0, len = gdjs.MapaCode.GDPlayerObjects1.length; i < len; ++i) {
          gdjs.MapaCode.GDPlayerObjects1[i].setX(384);
        }
      }
      {
        for (var i = 0, len = gdjs.MapaCode.GDPlayerObjects1.length; i < len; ++i) {
          gdjs.MapaCode.GDPlayerObjects1[i].setY(284);
        }
      }
    }
  }
  {
    gdjs.MapaCode.GDInimigoObjects1.length = 0;
    gdjs.MapaCode.condition0IsTrue_0.val = false;
    {
      {
        gdjs.MapaCode.conditionTrue_1 = gdjs.MapaCode.condition0IsTrue_0;
        gdjs.MapaCode.GDInimigoObjects1_1final.length = 0;
        gdjs.MapaCode.condition0IsTrue_1.val = false;
        gdjs.MapaCode.condition1IsTrue_1.val = false;
        gdjs.MapaCode.condition2IsTrue_1.val = false;
        {
          gdjs.copyArray(runtimeScene.getObjects("Inimigo"), gdjs.MapaCode.GDInimigoObjects2);

          for (var i = 0, k = 0, l = gdjs.MapaCode.GDInimigoObjects2.length; i < l; ++i) {
            if (gdjs.MapaCode.GDInimigoObjects2[i].getBehavior("PlatformerObject").isFalling()) {
              gdjs.MapaCode.condition0IsTrue_1.val = true;
              gdjs.MapaCode.GDInimigoObjects2[k] = gdjs.MapaCode.GDInimigoObjects2[i];
              ++k;
            }
          }

          gdjs.MapaCode.GDInimigoObjects2.length = k;

          if (gdjs.MapaCode.condition0IsTrue_1.val) {
            gdjs.MapaCode.conditionTrue_1.val = true;

            for (var j = 0, jLen = gdjs.MapaCode.GDInimigoObjects2.length; j < jLen; ++j) {
              if (gdjs.MapaCode.GDInimigoObjects1_1final.indexOf(gdjs.MapaCode.GDInimigoObjects2[j]) === -1) gdjs.MapaCode.GDInimigoObjects1_1final.push(gdjs.MapaCode.GDInimigoObjects2[j]);
            }
          }
        }
        {
          gdjs.copyArray(runtimeScene.getObjects("Inimigo"), gdjs.MapaCode.GDInimigoObjects2);

          for (var i = 0, k = 0, l = gdjs.MapaCode.GDInimigoObjects2.length; i < l; ++i) {
            if (gdjs.MapaCode.GDInimigoObjects2[i].getBehavior("PlatformerObject").isOnFloor()) {
              gdjs.MapaCode.condition1IsTrue_1.val = true;
              gdjs.MapaCode.GDInimigoObjects2[k] = gdjs.MapaCode.GDInimigoObjects2[i];
              ++k;
            }
          }

          gdjs.MapaCode.GDInimigoObjects2.length = k;

          if (gdjs.MapaCode.condition1IsTrue_1.val) {
            gdjs.MapaCode.conditionTrue_1.val = true;

            for (var j = 0, jLen = gdjs.MapaCode.GDInimigoObjects2.length; j < jLen; ++j) {
              if (gdjs.MapaCode.GDInimigoObjects1_1final.indexOf(gdjs.MapaCode.GDInimigoObjects2[j]) === -1) gdjs.MapaCode.GDInimigoObjects1_1final.push(gdjs.MapaCode.GDInimigoObjects2[j]);
            }
          }
        }
        {
          gdjs.MapaCode.condition2IsTrue_1.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 0;

          if (gdjs.MapaCode.condition2IsTrue_1.val) {
            gdjs.MapaCode.conditionTrue_1.val = true;
          }
        }
        {
          gdjs.copyArray(gdjs.MapaCode.GDInimigoObjects1_1final, gdjs.MapaCode.GDInimigoObjects1);
        }
      }
    }

    if (gdjs.MapaCode.condition0IsTrue_0.val) {
      {
        runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(0);
      }
    }
  }
  {
    gdjs.MapaCode.condition0IsTrue_0.val = false;
    {
      gdjs.MapaCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 0;
    }

    if (gdjs.MapaCode.condition0IsTrue_0.val) {
      gdjs.copyArray(runtimeScene.getObjects("Inimigo"), gdjs.MapaCode.GDInimigoObjects1);
      {
        for (var i = 0, len = gdjs.MapaCode.GDInimigoObjects1.length; i < len; ++i) {
          gdjs.MapaCode.GDInimigoObjects1[i].getBehavior("PlatformerObject").simulateControl("Left");
        }
      }
    }
  }
  {
    gdjs.MapaCode.condition0IsTrue_0.val = false;
    {
      gdjs.MapaCode.condition0IsTrue_0.val = !(gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 0);
    }

    if (gdjs.MapaCode.condition0IsTrue_0.val) {
      gdjs.copyArray(runtimeScene.getObjects("Inimigo"), gdjs.MapaCode.GDInimigoObjects1);
      {
        for (var i = 0, len = gdjs.MapaCode.GDInimigoObjects1.length; i < len; ++i) {
          gdjs.MapaCode.GDInimigoObjects1[i].getBehavior("PlatformerObject").simulateControl("Right");
        }
      }
    }
  }
};

gdjs.MapaCode.func = function (runtimeScene) {
  runtimeScene.getOnceTriggers().startNewFrame();
  gdjs.MapaCode.GDParedeObjects1.length = 0;
  gdjs.MapaCode.GDParedeObjects2.length = 0;
  gdjs.MapaCode.GDPlayerObjects1.length = 0;
  gdjs.MapaCode.GDPlayerObjects2.length = 0;
  gdjs.MapaCode.GDCoinObjects1.length = 0;
  gdjs.MapaCode.GDCoinObjects2.length = 0;
  gdjs.MapaCode.GDDeadLineObjects1.length = 0;
  gdjs.MapaCode.GDDeadLineObjects2.length = 0;
  gdjs.MapaCode.GDInimigoObjects1.length = 0;
  gdjs.MapaCode.GDInimigoObjects2.length = 0;
  gdjs.MapaCode.eventsList0(runtimeScene);
  return;
};

gdjs['MapaCode'] = gdjs.MapaCode;