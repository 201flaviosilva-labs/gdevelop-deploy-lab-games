const Links = [
	{
		name: "Plataforma",
		Web: "./src/Games/Plataforma/index.html",
		Code: "https://bitbucket.org/201flaviosilva/plataforma-gdevelop/",
	},
	{
		name: "SandBox",
		Web: "./src/Games/SandBox/index.html",
		Code: "https://bitbucket.org/201flaviosilva/gdevelop-sandbox/",
	},
	{
		name: "Super Create Box",
		Web: "./src/Games/SuperCreateBox/index.html",
		Code: "https://bitbucket.org/201flaviosilva/super-create-box-gdevelop/",
	},
	{
		name: "Top Down",
		Web: "./src/Games/TopDown/index.html",
		Windows: "https://bitbucket.org/201flaviosilva/top-down-gdevelop/downloads/TopDown-Windows.exe",
		Apple: "https://bitbucket.org/201flaviosilva/top-down-gdevelop/downloads/TopDown-MacOs.zip",
		// Linux: "https://mega.nz/file/rEkhHISY#0whwT__RsWZuIlSQmalhU_LePcYLv1pZCjf299wQjuA",
		Itch: "https://meiagaspea.itch.io/top-down-gdevelop",
		Code: "https://bitbucket.org/201flaviosilva/top-down-gdevelop/",
	},
];

export default Links;
